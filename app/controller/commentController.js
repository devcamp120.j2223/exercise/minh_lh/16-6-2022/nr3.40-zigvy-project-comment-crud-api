// import mongoose from './mongoose
const mongoose = require('mongoose');
// import model from './model   
const commentModel = require('../model/commentModel');
// Post create new user
const createComment = (request, response) => {
    let body = request.body;
    if (!body.name) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "name is required"
        })
    }
    if (!body.email) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "email is required"
        })
    }
    if (!body.body) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "body is required"
        })
    }
    //B3: Sư dụng cơ sở dữ liệu
    let commentCreate = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        email: body.email,
        body: body.body,
    }
    commentModel.create(commentCreate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create comment successfully",
                data: data
            })
        }
    })
}
// get all 
const getAllComment = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    commentModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all comments success",
                data: data
            })
        }
    })
}
// put all 
const putComment = (request, response) => {
    //B1: thu thập dữ liệu
    let commentId = request.params.commentId;
    let body = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "post ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updateComment = {
        name: body.name,
        email: body.email,
        body: body.body,
    }
    commentModel.findByIdAndUpdate(commentId, updateComment, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update comment success",
                data: data
            })
        }
    })
}
// getUserById all 
const CommentGetById = (request, response) => {
    //B1: thu thập dữ liệu
    let commentId = request.params.commentId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: " comment ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    commentModel.findById(commentId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get comment by id success",
                data: data
            })
        }
    })
}
// deleteUser all 
const deleteCommentById = (request, response) => {
    //B1: thu thập dữ liệu
    let commentId = request.params.commentId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: " comment ID is not valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    commentModel.findOneAndDelete(commentId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete comment success"
            })
        }
    })
}
// post of userid
//GET POSTS OF USER
const getCommentOfUser = (request, response) => {
    //B1: thu thập dữ liệu
    let postId = request.params.postId;

    let condition = {};

    if (postId) {
        condition.postId = postId;
    }

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "post ID is not valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    commentModel.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All Post Of User Id: " + postId,
                data: data
            })
        }
    })
}
//getAllPost
const getAllCommentQuery = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    let postId = request.query.postId

    commentModel.find(postId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all comment success",
                data: data
            })
        }
    })
}
//export 
module.exports = { createComment, getAllComment, putComment, CommentGetById, deleteCommentById, getCommentOfUser, getAllCommentQuery }