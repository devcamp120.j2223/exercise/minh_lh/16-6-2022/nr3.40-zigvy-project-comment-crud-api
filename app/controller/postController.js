// import mongoose from './mongoose
const mongoose = require('mongoose');
// import model from './model   
const postSchema = require('../model/postModel');
// Post create new user
const createPost = (request, response) => {
    let body = request.body;
    if (!body.title) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "title is required"
        })
    }
    if (!body.body) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "body is required"
        })
    }
    //B3: Sư dụng cơ sở dữ liệu
    let postCreate = {
        _id: mongoose.Types.ObjectId(),
        title: body.title,
        body: body.body,
    }
    postSchema.create(postCreate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create post successfully",
                data: data
            })
        }
    })
}
// get all 
const getAllPost = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    postSchema.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all post success",
                data: data
            })
        }
    })
}
// put all 
const putPost = (request, response) => {
    //B1: thu thập dữ liệu
    let postId = request.params.postId;
    let body = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "post ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updatePost = {
        title: body.title,
        body: body.body,
    }
    postSchema.findByIdAndUpdate(postId, updatePost, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update post success",
                data: data
            })
        }
    })
}
// getUserById all 
const PostGetById = (request, response) => {
    //B1: thu thập dữ liệu
    let postId = request.params.postId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Dice History ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    postSchema.findById(postId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get post by id success",
                data: data
            })
        }
    })
}
// deleteUser all 
const deletePost = (request, response) => {
    //B1: thu thập dữ liệu
    let postId = request.params.postId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Drink Id is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    postSchema.findOneAndDelete(postId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete post success"
            })
        }
    })
}
// post of userid
//GET POSTS OF USER
const getPostsOfUser = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId;
    }

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    postSchema.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All Post Of User Id: " + userId,
                posts: data
            })
        }
    })
}
//getAllPost
const getAllPostQuery = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    let userId = request.query.userId

    postSchema.find(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all post success",
                data: data
            })
        }
    })
}
//export 
module.exports = { createPost, getAllPost, putPost, PostGetById, deletePost, getPostsOfUser, getAllPostQuery }