// import mongoose from './mongoose
const mongoose = require('mongoose');
// import schema from './schema
const Schema = mongoose.Schema
// khai báo schema
const commentSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    body: {
        type: String,
        required: true,
    },
    postId: {
        type: mongoose.Types.ObjectId,
        ref: "post"
    },
});
//export model
module.exports = mongoose.model("comment", commentSchema);