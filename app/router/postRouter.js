// import express
const express = require('express');
// router
const router = express.Router();
// import middleware
const appMiddleware = require('../middleware/appMiddleware');
//import controllers
const { createPost, getAllPost, putPost, PostGetById, deletePost, getPostsOfUser,getAllPostQuery } = require('../controller/postController');
// sử dụng middleware
router.use(appMiddleware);
// Post
router.post("/posts", createPost);
//Get
router.get("/posts", getAllPost);
//Put
router.put("/posts/:postId", putPost);
//Get by id
router.get("/posts/:postId", PostGetById);
//Delete
router.delete("/posts/:postId", deletePost);
// getPostsOfUser
router.get("/users/:userId/posts", getPostsOfUser);
// getAllPost
router.get("/posts", getAllPostQuery);
//export
module.exports = router;