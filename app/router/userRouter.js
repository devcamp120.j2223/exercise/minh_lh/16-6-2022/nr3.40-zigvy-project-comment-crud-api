// import express
const express = require('express');
// router
const router = express.Router();
// import middleware
const appMiddleware = require('../middleware/appMiddleware');
//import controllers
const { postUser, getUser, putUser, getById, deleteUser, } = require('../controller/userController');
// sử dụng middleware
router.use(appMiddleware);
// Post
router.post("/users", postUser);
//Get
router.get("/users", getUser);
//Put
router.put("/users/:userId", putUser);
//Get by id
router.get("/users/:userId", getById);
//Delete
router.delete("/users/:userId", deleteUser);
//export
module.exports = router;