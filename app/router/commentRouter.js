// import express
const express = require('express');
// router
const router = express.Router();
// import middleware
const appMiddleware = require('../middleware/appMiddleware');
//import controllers
const { createComment, getAllComment, putComment, CommentGetById, deleteCommentById, getCommentOfUser, getAllCommentQuery } = require('../controller/commentController');
// sử dụng middleware
router.use(appMiddleware);
// Post
router.post("/comments", createComment);
//Get
router.get("/comments", getAllComment);
//Put
router.put("/comments/:commentId", putComment);
//Get by id
router.get("/comments/:commentId", CommentGetById);
//Delete
router.delete("/comments/:commentId", deleteCommentById);
// getPostsOfUser
router.get("/posts/:postId/comments", getCommentOfUser);
// getAllPost
router.get("/comments", getAllCommentQuery);
//export
module.exports = router;